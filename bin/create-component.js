//#!/usr/bin/env node
const path = require('path');
const fs = require('fs');

//const args = process.argv.slice(2);
const toCamelCase = str => str.replace(/-./g, c => c.substring(1).toUpperCase());
const toPascalCase = str => str.replace(/\w\S*/g, m => m.charAt(0).toUpperCase() + m.substr(1));
//const componentName = args[0];
//args.includes('--redux')

const createComponent = options => {
  const projectRoot = process.cwd();
  const { componentName, isRedux } = options;
  const componentNameCamelCase = toCamelCase(componentName);
  const componentNamePascalCase = toPascalCase(componentNameCamelCase);
  const componentDir = path.resolve(projectRoot, `./src/javascripts/components/${componentName}`);

  const jsxTemplate = componentName => `import React from 'react';

const ${componentName} = () => {};

export default ${componentName};
`;

  const indexTemplate = componentName => `import ${toPascalCase(
  toCamelCase(componentName)
)} from './${componentName}';

export default ${toPascalCase(toCamelCase(componentName))};`;

  const indexTemplateRedux = componentName => `import { connect } from 'react-redux';
import { compose } from 'redux';
import ${toPascalCase(toCamelCase(componentName))} from './${componentName}';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(${toPascalCase(toCamelCase(componentName))});
`;

  const reducerTemplate = componentName => `import actions from './actions';

const initialState = {
};

const ${componentName} = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    default:
      return state;
  }
};

export default ${componentName};
`;

  const actionsTemplate = () => `const actions = {};

export default actions;
`;

  const simpleFiles = [
    {
      name: `${componentName}.jsx`,
      template: jsxTemplate(componentNamePascalCase),
    },
  ];

  const reduxFiles = [
    {
      name: 'index.js',
      template: indexTemplateRedux(componentName),
    },
    {
      name: 'reducer.js',
      template: reducerTemplate(componentNameCamelCase),
    },
    {
      name: 'actions.js',
      template: actionsTemplate(),
    }
  ];

  let files;
  if (isRedux) {
    files = [...simpleFiles, ...reduxFiles];
  } else {
    files = [
      ...simpleFiles,
      {
        name: 'index.js',
        template: indexTemplate(componentName),
      },
    ];
  }

  fs.mkdirSync(componentDir);
  files.forEach(file => {
    fs.writeFileSync(`${componentDir}/${file.name}`, file.template);
  });
};

exports.createComponent = createComponent;
