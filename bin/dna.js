#!/usr/bin/env node

const args = process.argv.slice(2);

const scriptIndex = args.findIndex(
  x => x === 'build' || x === 'init' || x === 'dev' || x === 'test' || x === 'create-component'
);
const script = scriptIndex === -1 ? args[0] : args[scriptIndex];

process.env.BABEL_ENV = script === 'dev' ? 'development' : 'production';
process.env.NODE_ENV = script === 'dev' ? 'development' : 'production';

const path = require('path');
const fs = require('fs-extra');
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const formatWebpackMessages = require('react-dev-utils/formatWebpackMessages');
const { networkInterfaces } = require('os');
const { createComponent } = require('./create-component.js');

const packageRoot = path.resolve(__dirname, '../');
const projectRoot = process.cwd();

const getIP = () => {
  const nets = networkInterfaces();
  const results = Object.create(null); // or just '{}', an empty object

  for (const name of Object.keys(nets)) {
    for (const net of nets[name]) {
      // skip over non-ipv4 and internal (i.e. 127.0.0.1) addresses
      if (net.family === 'IPv4' && !net.internal) {
        if (!results[name]) {
          results[name] = [];
        }

        // results[name].push(net.address);
        if (net.address.includes('192.168')) {
          return net.address;
        }
      }
    }
  }
  return results;
};

const init = () => {
  console.log('Copying files...');

  const copy = [
    './src',
    './web',
    './etc',
    './docker-compose.yml',
    './.nvmrc',
    './.editorconfig',
    './.eslintrc',
    './.stylelintrc',
    './.purgecss-whitelist.js',
    './.gitignore',
    './README.md',
    './example.env',
  ];

  copy.forEach(i => {
    const src = path.resolve(packageRoot, i);
    const dest = path.resolve(projectRoot, i);
    fs.copy(src, dest).catch(err => console.error(err));
  });

  console.log('Done', '\n');
};

const dev = () => {
  console.log('Starting WebpackDevServer...');
  const devConfig = require('../webpack/dev.js');
  const compiler = webpack(devConfig);
  const server = new WebpackDevServer(compiler, devConfig.devServer);
  const { port, host, https } = devConfig.devServer;

  server.listen(port, host, err => {
    if (err) {
      console.log(err);
    }
    const localIP = getIP();
    console.log(`\n==========\nWebpackDevServer listening at ${https ? 'https' : 'http'}://${host}:${port}`);
    if (typeof localIP === 'string') {
      console.log(`Access on LAN devices at ${https ? 'https' : 'http'}://${localIP}:${port}\n==========\n`);
    }
  });
};

const build = () => {
  const prodConfig = require('../webpack/prod.js');
  const compiler = webpack(prodConfig);
  let messages;
  compiler.run((err, stats) => {
    if (err) {
      if (!err.message) {
        process.exit(1);
      }

      let errMessage = err.message;

      // Add additional information for postcss errors
      if (Object.prototype.hasOwnProperty.call(err, 'postcssNode')) {
        errMessage +=
          '\nCompileError: Begins at CSS selector ' +
          err['postcssNode'].selector;
      }

      messages = formatWebpackMessages({
        errors: [errMessage],
        warnings: [],
      });
    } else {
      messages = formatWebpackMessages(stats.toJson({ all: false, warnings: true, errors: true }));
    }
    if (messages.errors.length) {
      // Only keep the first error. Others are often indicative
      // of the same problem, but confuse the reader with noise.
      if (messages.errors.length > 1) {
        messages.errors.length = 1;
      }
      console.log(messages.errors.join('\n\n'));
      process.exit(1);
    }
  });
};

switch (script) {
  case 'init':
    init();
    break;
  case 'dev':
    dev();
    break;
  case 'build':
    build();
    break;
  case 'create-component':
    const options = {
      componentName: args[1],
      isRedux: args.includes('--redux')
    };
    createComponent(options);
    break;
  default:
    console.log('No arguments given');
}
