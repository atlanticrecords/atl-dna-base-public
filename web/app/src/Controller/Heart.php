<?php
namespace App\Controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Schema\Blueprint;
use \Ramsey\Uuid\Uuid;

class Heart {
  protected $database;

  public function __construct(Manager $database) {
    $this->database = $database;
  }

  public function fetchHearts(Request $request) {
    $columns = [
      'uuid',
      'locations',
      'created_at',
      'updated_at',
    ];

    $collection = $this->database
      ->table('hearts')
      ->get($columns);

    // Convert location JSON to array
    foreach ($collection as $heart) {
      $heart->locations = json_decode($heart->locations);
    }

    return $collection;
  }

  public function insertHeart(Request $request) {
    $location = $request->getParsedBodyParam('location');

    if (!$location) {
      throw new \Exception('$location is empty', 400);
    }

    $date = new \DateTime();
    $timestamp = $date->format('Y-m-d G:i:s');
     
    $uuid1 = Uuid::uuid1();
    $uuid = $uuid1->toString();

    $model = new \stdClass;
    $model->uuid = $uuid;
    $model->locations = json_encode(array($location));
    $model->created_at = $timestamp;

    $result = $this->database
      ->table('hearts')
      ->insert([
        'uuid' => $model->uuid,
        'locations' => $model->locations,
        'created_at' => $model->created_at
      ]);

    if (!$result) {
      throw new \Exception('Insert row failed', 400);
    }

    // Convert json storage back before sending
    $model->locations = json_decode($model->locations);

    return $model;
  }

  public function updateHeart(Request $request) {
    $columns = [
      'uuid',
      'locations',
      'created_at',
      'updated_at',
    ];

    $uuid = $request->getParam('uuid');
    $location = $request->getParsedBodyParam('location');

    if (!$uuid) {
      throw new \Exception('$uuid is empty', 400);
    }

    if (!$location) {
      throw new \Exception('$location is empty', 400);
    }

    $heart = $this->database
      ->table('hearts')
      ->where('uuid', $uuid)
      ->first($columns);

    if (!$heart) {
      throw new \Exception('No heart found', 400);
    }

    $date = new \DateTime();
    $timestamp = $date->format('Y-m-d G:i:s');

    $locations = json_decode($heart->locations);
    array_unshift($locations , $location);

    $heart->updated_at = $timestamp;
    $heart->locations = json_encode($locations);

    $result = $this->database
      ->table('hearts')
      ->where('uuid', $heart->uuid)
      ->update([
        'updated_at' => $heart->updated_at,
        'locations' =>  $heart->locations,
      ]);

    if (!$result) {
      throw new \Exception('Update row failed', 400);
    }

    $heart->locations = json_decode($heart->locations);

    return $heart;
  }

  public function up() {
    $this->database->schema()->create('hearts', function (Blueprint $table) {
      $table->uuid('uuid')->primary();
      $table->json('locations');
      $table->timestamps();
    });
  }

  public function down() {
    $this->database->schema()->drop('hearts');
  }
}