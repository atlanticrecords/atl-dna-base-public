<?php
namespace App\Controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Intervention\Image\ImageManager;
use \Exception;

class Image {

  public function saveImage(Request $request, $directory) {
    $manager = new ImageManager(array('driver' => 'gd'));
    $savedImages = [];
    $result['status'] = 'failed';

    $uploadedFiles = $request->getUploadedFiles();

    if (empty($uploadedFiles)) {
      $result['message'] = 'No files provided';
      return json_encode($result);
    }

    foreach ($uploadedFiles as $file) {
      if ($file->getError() === UPLOAD_ERR_OK) {
        try {
          $fileId = md5(uniqid(rand(), true));
          $filename = $fileId . '.jpg';
          $img = $manager->make($file->file)->save($directory . '/' . $filename);

          $fileResponse = [];
          $fileResponse['filename'] = $filename;
          $fileResponse['file_id'] = $fileId;
          $savedImages[] = $fileResponse;
        } catch (Exception $e) {
          $result['status'] = 'error';
          $result['message'] = $e->getMessage();
        }
      }
    }

    if (count($savedImages) > 0) {
      $result['status'] = 'success';
      $result['items'] = $savedImages;
    }

    return $result;
  }
}
