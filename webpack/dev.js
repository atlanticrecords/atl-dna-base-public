// const glob = require('glob-all');
const path = require('path');
const merge = require('webpack-merge');
// const resolve = require('resolve');

const webpack = require('webpack');
const sane = require('sane');
const WebpackNotifierPlugin = require('webpack-notifier');
const globImporter = require('node-sass-glob-importer');
const autoprefixer = require('autoprefixer');
const sass = require('sass');
const DataReloadPlugin = require('./data-reload-plugin');
// const Dashboard = require('webpack-dashboard');
// const DashboardPlugin = require('webpack-dashboard/plugin');
const common = require('./common.js');
// const dashboard = new Dashboard();

const templateDataPath = path.resolve(process.cwd(), './src/html/data.json');

const processCwd = process.cwd();

const configureDevServer = () => ({
  contentBase: path.resolve(processCwd, process.env.PUBLIC_PATH),
  historyApiFallback: true,
  hot: true,
  host: process.env.DEVSERVER_HOST,
  // hotOnly: true,
  https: !!Number(process.env.DEVSERVER_HTTPS),
  noInfo: true,
  open: true,
  overlay: true,
  port: process.env.DEVSERVER_PORT,
  proxy: {
    '/api': `http://${process.env.NGINX_HOST}:${process.env.NGINX_PORT}`,
  },
  quiet: true,
  stats: 'errors-only',
  after: (app, server, compiler) => {
    // console.log(server);
    compiler.hooks.done.tapAsync('DataReloadPlugin', async (compilation, callback) => {
      // Only look out for changed html files.
      // const watchFiles = ['.html', '.json'];
      const changedFiles = Object.keys(compiler.watchFileSystem.watcher.mtimes);
      // console.log(changedFiles, path.parse( templateDataPath ));
      // Send 'content-changed' socket message to manually tigger liveReload.
      // changedFiles.some(filePath => watchFiles.includes(path.parse(filePath).ext))
      if (server.options.hot && changedFiles.includes(templateDataPath)) {
        server.sockWrite(server.sockets, 'content-changed');
      }

      callback();
    });
  },
  // watchOptions: {
  //   poll: 1,
  //   ignored: /node_modules/
  // },
  // before: (app, server) => {
  //   // devServer = server;
  //   const watcher = sane(path.resolve(processCwd, './src/html'), {
  //     glob: ['**/*.json'],
  //     // watchman: true
  //   });
  //   watcher.on('change', (filePath, root, stat) => {
  //     console.log('  File modified:', filePath);
  //     server.sockWrite(server.sockets, 'content-changed');
  //   });
  // },
});

// Configure Image loader
const configureImageLoader = () => ({
  test: /\.(png|jpe?g|gif|svg|webp)$/i,
  use: [
    {
      loader: require.resolve('file-loader'),
      options: {
        name: 'img/[name].[ext]',
      },
    },
  ],
});

// Configure Audio loader
const configureAudioLoader = () => ({
  test: /\.(mp3|ogg)$/,
  use: [
    {
      loader: require.resolve('file-loader'),
      options: {
        name: 'audio/[name].[ext]',
      },
    },
  ],
});

// Configure Video loader
const configureVideoLoader = () => ({
  test: /\.(mp4|mov|webm|mkv)$/,
  use: [
    {
      loader: require.resolve('file-loader'),
      options: {
        name: 'video/[name].[ext]',
      },
    },
  ],
});

// Configure Font loader
const configureFontLoader = () => ({
  test: /\.(ttf|eot|otf|woff2?)$/i,
  use: [
    {
      loader: require.resolve('file-loader'),
      options: {
        name: 'fonts/[name].[ext]',
      },
    },
  ],
});

const configureSassLoader = () => ({
  test: /\.scss$/,
  use: [
    require.resolve('style-loader'),
    {
      loader: require.resolve('css-loader'),
      options: {
        importLoaders: 4,
        sourceMap: true,
      },
    },
    {
      loader: require.resolve('resolve-url-loader'),
    },
    {
      loader: require.resolve('postcss-loader'),
      options: {
        sourceMap: true,
        plugins: () => [autoprefixer],
      },
    },
    {
      loader: require.resolve('sass-loader'),
      options: {
        implementation: sass,
        sassOptions: {
          importer: globImporter(),
        },
        sourceMap: true,
      },
    },
    {
      loader: require.resolve('sass-resources-loader'),
      options: {
        sourceMap: true,
        resources: [
          'node_modules/bootstrap/scss/_functions.scss',
          'node_modules/bootstrap/scss/_variables.scss',
          'node_modules/bootstrap/scss/mixins/_breakpoints.scss',
          'src/stylesheets/_settings.scss',
        ],
      },
    },
  ],
});

module.exports = merge(common, {
  mode: 'development',
  devtool: 'eval-source-map',
  devServer: configureDevServer(),
  output: {
    filename: path.join('./js', '[name].js'),
    path: path.resolve(processCwd, process.env.PUBLIC_PATH),
    publicPath: '/',
  },
  plugins: [
    new WebpackNotifierPlugin({ title: 'Webpack', excludeWarnings: true, alwaysNotify: true }),
    new DataReloadPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    // new DashboardPlugin(dashboard.setData),
  ],
  module: {
    rules: [configureSassLoader(), configureImageLoader(), configureAudioLoader(), configureFontLoader(), configureVideoLoader()],
  },
});
