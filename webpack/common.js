// node modules
const path = require('path');
const resolve = require('resolve');
require('dotenv').config({
  path: path.resolve(process.cwd(), './.env'),
});

// webpack plugins
const CopyPlugin = require('copy-webpack-plugin');
const IconfontPlugin = require('iconfont-plugin-webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');

// config files
const pkg = require('../package.json');

const templateDataPath = path.resolve(process.cwd(), './src/html/data.json');
const templateData = require(templateDataPath);

const processCwd = process.cwd();

// Configure Babel loader
const configureBabelLoader = () => ({
  test: /\.(js|jsx)$/,
  exclude: /node_modules/,
  use: {
    loader: require.resolve('babel-loader'),
    options: {
      presets: [
        [
          require.resolve('@babel/preset-env'),
          {
            debug: false,
            modules: false,
            useBuiltIns: 'usage',
            corejs: {
              version: '3.6',
              proposals: true,
            },
            targets: {
              browsers: Object.values(
                process.env.NODE_ENV === 'development'
                  ? pkg.browserslist.development
                  : pkg.browserslist.production
              ),
            },
          },
        ],
        require.resolve('@babel/preset-react'),
      ],
      plugins: [
        require.resolve('@babel/plugin-syntax-dynamic-import'),
        require.resolve('@babel/plugin-proposal-export-default-from'),
        require.resolve('@babel/plugin-proposal-class-properties'),
        require.resolve('react-hot-loader/babel'),
        [
          require.resolve('@babel/plugin-transform-runtime'),
          {
            regenerator: true,
          },
        ],
      ],
    },
  },
});

module.exports = {
  name: pkg.name,
  entry: {
    app: path.resolve(processCwd, './src/javascripts/index.js'),
  },
  resolve: {
    alias: {
      components: path.resolve(processCwd, './src/javascripts/components'),
      constants: path.resolve(processCwd, './src/javascripts/constants'),
      utils: path.resolve(processCwd, './src/javascripts/utils'),
      images: path.resolve(processCwd, './src/images'),
      audio: path.resolve(processCwd, './src/audio'),
      video: path.resolve(processCwd, './src/video'),
      stylesheets: path.resolve(processCwd, './src/stylesheets'),
    },
    modules: ['node_modules'],
    extensions: ['.js', '.json', '.jsx', '.scss', '*'],
  },
  module: {
    rules: [configureBabelLoader()],
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(processCwd, './src/static/**/*'),
          transformPath(targetPath) {
            const search = 'src/static/';
            const index = targetPath.lastIndexOf(search);
            return targetPath.substring(index + search.length);
          },
        },
      ],
    }),
    new HtmlWebPackPlugin({
      template: path.resolve(processCwd, './src/html/index.ejs'),
      templateParameters: templateData,
      cache: false,
    }),
    new IconfontPlugin({
      src: './src/icons', // required - directory where your .svg files are located
      family: 'atl-iconfont', // optional - the `font-family` name. if multiple iconfonts are generated, the dir names will be used.
      dest: {
        font: './src/fonts/generated/[family].[type]', // required - paths of generated font files
        css: './src/stylesheets/generated/_iconfont_[family].scss', // required - paths of generated css files
      },
      watch: {
        pattern: 'src/icons/**/*.svg', // required - watch these files to reload
        // cwd: undefined // optional - current working dir for watching
      },
      // cssTemplate: function() {}// optional - the function to generate css contents
    }),
  ],
};
