// const SingleEntryPlugin = require('webpack/lib/SingleEntryPlugin');
const path = require('path');
const fs = require('fs');
const HtmlWebPackPlugin = require('html-webpack-plugin');

const templateDataPath = path.resolve(process.cwd(), './src/html/data.json');

class DataReloadPlugin {
  apply(compiler) {
    // compiler.hooks.done.tap('DataReload', () => {});
    // compiler.addEntry(templateDataPath);

    compiler.hooks.emit.tapAsync('DataReloadPlugin', (compilation, callback) => {
      compilation.fileDependencies.add(templateDataPath);
      callback();
    });

    // compiler.hooks.shouldEmit.tap('DataReload', compilation => {
    //   //console.log(compilation);
    //   // console.log('Assets', compilation.getAssets().map(asset => ({ name: asset.name, path: asset.source.existsAt })));
    //   // console.log('Assets', compilation.getAssets());
    //   console.log(HtmlWebPackPlugin);
    // });

    compiler.hooks.make.tapAsync('DataReloadPlugin', (compilation, callback) => {
      // console.log('Compiler starting compilation', compilation);
      // const childCompiler = compilation.createChildCompiler('DataReload');
      // // Everyone plugin does this, I don't know why
      // childCompiler.context = compiler.context;

      // console.log(childCompiler.hooks);
      // childCompiler.hooks.initialize.tap(
      //   () => new SingleEntryPlugin(compiler.context, templateDataPath, 'data.json'));

      // // Needed for HMR. Even if your plugin don't support HMR,
      // // this code seems to be always needed just in case to prevent possible errors
      // childCompiler.plugin('compilation', (compilation) => {
      //   if (compilation.cache) {
      //     // console.log(compilation.cache);
      //     if (!compilation.cache[templateDataPath]) {
      //       compilation.cache[templateDataPath] = {};
      //     }

      //     compilation.cache = compilation.cache[templateDataPath];
      //   }
      // });

      HtmlWebPackPlugin.getHooks(compilation).alterAssetTagGroups.tapAsync(
        'DataReload',
        (data, cb) => {
          fs.readFile(templateDataPath, (err, file) => {
            const newData = JSON.parse(file);
            const dataCopy = { ...data };
            // console.log('HTML Webpack Plugin Data', data);
            dataCopy.plugin.options.templateParameters = newData;
            cb(null, dataCopy);
          });
        }
      );

      callback();
      // Run child compilation
      // childCompiler.runAsChild((err, entries, childCompilation) => {
      //   callback(err);
      // });
    });
  }
}

module.exports = DataReloadPlugin;
