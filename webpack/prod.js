const glob = require('glob-all');
const path = require('path');
const merge = require('webpack-merge');

const globImporter = require('node-sass-glob-importer');
const sass = require('sass');
const autoprefixer = require('autoprefixer');

const ManifestPlugin = require('webpack-manifest-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const PurgecssPlugin = require('purgecss-webpack-plugin');
const WhitelisterPlugin = require('purgecss-whitelister');
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const common = require('./common.js');

const processCwd = process.cwd();

let purgeCssLocalWhitelist = null;

try {
  purgeCssLocalWhitelist = require(path.resolve(processCwd, './.purgecss-whitelist.js'));
  // console.log(purgeCssLocalWhitelist);
} catch (e) {
  console.log('No local purgecss found');
}

const configureOptimization = () => ({
  splitChunks: {
    chunks: 'all',
    cacheGroups: {
      default: false,
      common: false,
      styles: {
        name: 'styles',
        test: /\.s?css$/,
        chunks: 'all',
        minChunks: 1,
        reuseExistingChunk: true,
        enforce: true,
      },
    },
  },
  minimizer: [
    new TerserPlugin({
      cache: true,
      parallel: true,
      sourceMap: true,
    }),
    new OptimizeCSSAssetsPlugin({
      cssProcessorOptions: {
        map: {
          inline: false,
          annotation: true,
        },
        safe: true,
        discardComments: true,
      },
    }),
  ],
});

const configureImageLoader = () => ({
  test: /\.(png|svg|jpe?g|gif|webp)$/i,
  use: [
    {
      loader: require.resolve('file-loader'),
      options: {
        name: 'img/[name].[hash].[ext]',
      },
    },
    // {
    //   loader: require.resolve('img-loader'),
    //   options: {
    //     plugins: [
    //       require('imagemin-gifsicle')({
    //         interlaced: true,
    //       }),
    //       require('imagemin-mozjpeg')({
    //         progressive: true,
    //         arithmetic: false,
    //       }),
    //       require('imagemin-optipng')({
    //         optimizationLevel: 5,
    //       }),
    //       require('imagemin-svgo')({
    //         plugins: [
    //           {convertPathData: false},
    //         ]
    //       }),
    //     ]
    //   }
    // }
  ],
});

const configureSassLoader = () => ({
  test: /\.scss$/,
  use: [
    {
      loader: MiniCssExtractPlugin.loader,
      options: {
        publicPath: '../',
      },
    },
    {
      loader: require.resolve('css-loader'),
      options: {
        importLoaders: 4,
        sourceMap: true,
      },
    },
    {
      loader: require.resolve('resolve-url-loader'),
    },
    {
      loader: require.resolve('postcss-loader'),
      options: {
        sourceMap: true,
        plugins: () => [autoprefixer],
      },
    },
    {
      loader: require.resolve('sass-loader'),
      options: {
        implementation: sass,
        sassOptions: {
          importer: globImporter(),
        },
        sourceMap: true,
      },
    },
    {
      loader: require.resolve('sass-resources-loader'),
      options: {
        sourceMap: true,
        resources: [
          'node_modules/bootstrap/scss/_functions.scss',
          'node_modules/bootstrap/scss/_variables.scss',
          'node_modules/bootstrap/scss/mixins/_breakpoints.scss',
          'src/stylesheets/_settings.scss',
        ],
      },
    },
  ],
});

const purgeCssConfig = {
  paths: ['./src/javascripts/**/*.{jsx,js}'],
  whitelist: ['./src/stylesheets/**/*.{css,scss}'],
  whitelistPatterns: [/atl-.*/],
  extensions: ['html', 'js'],
};

// Configure PurgeCSS
const configurePurgeCss = () => {
  const paths = [];
  // Configure purge paths
  Object.entries(purgeCssConfig.paths).map(([, value]) =>
    paths.push(path.resolve(processCwd, value))
  );

  // Configure whitelist paths
  let whitelistTmp = [...purgeCssConfig.whitelist];
  const whitelist = [];
  if (purgeCssLocalWhitelist) {
    whitelistTmp = whitelistTmp.concat(purgeCssLocalWhitelist.whitelist);
  }

  whitelistTmp.forEach(value => whitelist.push(path.resolve(processCwd, value)));

  // console.log(WhitelisterPlugin(glob.sync(whitelist)));

  return {
    paths: glob.sync(paths),
    whitelist: WhitelisterPlugin(glob.sync(whitelist)),
    whitelistPatterns: purgeCssConfig.whitelistPatterns,
    extractors: [
      {
        extractor: content => content.match(/[A-Za-z0-9-_:\\/]+/g) || [],
        extensions: ['js', 'jsx'], // file extensions
      },
    ],
  };
};

// Configure Manifest
const configureManifest = fileName => ({
  fileName,
  basePath: '',
  map: file => {
    const clone = { ...file };
    clone.name = file.name.replace(/(\.[a-f0-9]{32})(\..*)$/, '$2');
    return clone;
  },
});

// Configure Audio loader
const configureAudioLoader = () => ({
  test: /\.(mp3|ogg)$/,
  use: [
    {
      loader: require.resolve('file-loader'),
      options: {
        name: 'audio/[name].[hash].[ext]',
      },
    },
  ],
});

// Configure Video loader
const configureVideoLoader = () => ({
  test: /\.(mp4|mov|webm|mkv)$/,
  use: [
    {
      loader: require.resolve('file-loader'),
      options: {
        name: 'video/[name].[hash].[ext]',
      },
    },
  ],
});

// Configure Font loader
const configureFontLoader = () => ({
  test: /\.(ttf|eot|otf|woff2?)$/i,
  use: [
    {
      loader: require.resolve('file-loader'),
      options: {
        name: 'fonts/[name].[hash].[ext]',
      },
    },
  ],
});

module.exports = merge(common, {
  mode: 'production',
  devtool: 'source-map',
  optimization: configureOptimization(),
  output: {
    filename: path.join('./js', '[name].[hash].js'),
    path: path.resolve(processCwd, process.env.PUBLIC_PATH),
    publicPath: process.env.BASE_NAME + '/',
  },
  module: {
    rules: [configureSassLoader(), configureImageLoader(), configureAudioLoader(), configureFontLoader(), configureVideoLoader()],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: path.join('./css', '[name].css'),
      chunkFilename: path.join('./css', '[name].[hash].css'),
    }),
    new PurgecssPlugin(configurePurgeCss()),
    new ManifestPlugin(configureManifest('manifest.json')),
  ],
});
