import SpotifyWebApi from 'spotify-web-api-js';
import { SPOTIFY_CONFIG } from 'utils/config';
import { parse } from 'query-string';
import { scrapeEmail } from 'utils/mailing-list';

const defaults = {
  followArtists: true,
  followPlaylist: true,
  addTracks: false,
  scrapeEmail: true,
};

class Spotify {
  constructor(settings) {
    this.settings = { ...defaults, ...settings };
    this.api = new SpotifyWebApi();
    this.watchForRedirect();
  }

  watchForRedirect() {
    const accessTokenOnRedirect = parse(location.search).access_token;

    if (accessTokenOnRedirect) {
      this.authorize(accessTokenOnRedirect);
    }
  }

  authorize(token) {
    this.api.setAccessToken(token);

    // Scrape user data
    this.api.getMe().then(this.init.bind(this));
  }

  init(user) {
    this.user = user;

    // Follow artist
    if (SPOTIFY_CONFIG.artistId && this.settings.followArtists) {
      this.api.followArtists([SPOTIFY_CONFIG.artistId]);
    }

    // Follow playist
    if (SPOTIFY_CONFIG.playlistId && this.settings.followPlaylist) {
      this.api.followPlaylist(SPOTIFY_CONFIG.playlistId);
    }

    // Save tracks
    if (SPOTIFY_CONFIG.tracks && this.settings.addTracks) {
      let tracks = (SPOTIFY_CONFIG.tracks instanceof Array) ? SPOTIFY_CONFIG.tracks : [SPOTIFY_CONFIG.tracks];
      this.api.addToMySavedTracks(tracks);
    }

    // Mailing list
    if (this.settings.scrapeEmail && user.email) {
      scrapeEmail(user.email);
    }
  }
}

export default Spotify;
