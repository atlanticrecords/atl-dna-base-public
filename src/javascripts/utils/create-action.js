export default function createAction(type) {
  return (payload = {}) => dispatch => dispatch({
    type,
    payload,
  });
}
