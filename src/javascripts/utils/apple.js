import axios from 'axios';

import { APPLE_CONFIG } from 'utils/config';

const APPLE_API_ENDPOINT = 'https://api.music.apple.com/v1/';

const defaults = {
  addTracks: true,
};

class Apple {
  constructor(token, settings) {
    this.settings = { ...defaults, ...settings };
    this.api = axios.create({
      baseURL: APPLE_API_ENDPOINT,
      headers: {
        'Authorization': `Bearer ${APPLE_CONFIG.clientId}`,
        'Music-User-Token': token,
      },
    });

    this.init();
  }

  init() {
    // Save tracks
    if (APPLE_CONFIG.tracks && this.settings.addTracks) {
      this.api.post(`/me/library?ids[songs]=${APPLE_CONFIG.tracks}`);
    }
  }
}

export default Apple;
