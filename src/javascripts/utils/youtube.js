import { YOUTUBE_CONFIG } from 'utils/config';
import { scrapeEmail } from 'utils/mailing-list';

const defaults = {
  followChannel: true,
  scrapeEmail: true,
};

class Youtube {
  constructor(email, settings) {
    this.settings = { ...defaults, ...settings };
    this.api = gapi.client.youtube;
    this.email = email;
    this.init();
  }

  init() {
    // Follow channel
    if (YOUTUBE_CONFIG.channelId && this.settings.followChannel) {
      this.followChannel();
    }

    // Mailing list
    if (this.settings.scrapeEmail && this.email) {
      scrapeEmail(this.email)
    }
  }

  followChannel() {
    const resource = {
      part: 'snippet',
      snippet: {
        resourceId: {
          kind: 'youtube#channel',
          channelId: YOUTUBE_CONFIG.channelId,
        }
      }
    };

    this.api.subscriptions.insert(resource).execute();
  }
}

export default Youtube;
