import React from 'react';
import PropTypes from 'prop-types';

const TopsifyAuthBtn = ({ btnText, service, fetchTopsifyLogin, validate, onAuthSuccess, useIcon }) => (
  <button
    type="button"
    className="btn btn-primary"
    data-track={`${service}-follow`}
    disabled={!validate()}
    onClick={() => fetchTopsifyLogin({ service, onAuthSuccess })}
  >
    {useIcon && <i className={`atl-icon atl-icon--${service}`} />}
    <span className={`${useIcon ? 'ml-1' : ''}`}>{btnText}</span>
  </button>
);

TopsifyAuthBtn.defaultProps = {
  validate: () => true,
  onAuthSuccess: () => true,
  btnText: 'Login',
  useIcon: true,
};

TopsifyAuthBtn.propTypes = {
  fetchTopsifyLogin: PropTypes.func.isRequired,
  service: PropTypes.string.isRequired,
  validate: PropTypes.func,
  btnText: PropTypes.string,
  onAuthSuccess: PropTypes.func,
  useIcon: PropTypes.bool,
};

export default TopsifyAuthBtn;
