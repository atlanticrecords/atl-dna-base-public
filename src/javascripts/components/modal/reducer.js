import modalActions from './actions';

const initialState = {
  show: false,
  type: null,
  additionalProps: null,
};

const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case modalActions.MODAL_SHOW:
      return { ...state, ...action.payload, show: true };
    case modalActions.MODAL_HIDE:
      return { ...state, show: false };
    default:
      return state;
  }
};

export default modalReducer;
