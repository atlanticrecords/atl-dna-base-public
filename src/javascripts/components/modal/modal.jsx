import React from 'react';
import PropTypes from 'prop-types';

const Modal = ({ type, hideModal, closeButton }) => {
  const ModalComponent = type;

  return (
    <div className="modal">
      <div className="modal__content">{type && <ModalComponent />}</div>
      {closeButton && (
        <button
          className="modal__close-button"
          custom-link-name="Close Lightbox"
          onClick={() => hideModal()}
        >
          <i className="icon close" />
        </button>
      )}
    </div>
  );
};

Modal.defaultProps = {
  type: null,
  closeButton: true,
};

Modal.propTypes = {
  type: PropTypes.func,
  hideModal: PropTypes.func.isRequired,
  closeButton: PropTypes.bool,
};

export default Modal;
