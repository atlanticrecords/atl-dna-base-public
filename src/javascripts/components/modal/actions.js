const actions = {
  MODAL_SHOW: 'MODAL_SHOW',
  MODAL_HIDE: 'MODAL_HIDE',
};

const showModal = modal => dispatch => dispatch({ type: actions.MODAL_SHOW, payload: modal });
const hideModal = () => dispatch => dispatch({ type: actions.MODAL_HIDE });

export default actions;
export { showModal, hideModal };
