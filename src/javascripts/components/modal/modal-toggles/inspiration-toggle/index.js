import { connect } from 'react-redux';

import { showModal } from 'components/modal/actions';
import InspirationToggle from './inspiration-toggle';

const mapDispatchToProps = dispatch => ({
  showInspiration: () => dispatch(showModal({ type: 'Inspiration' })),
});

export default connect(null, mapDispatchToProps)(InspirationToggle);
