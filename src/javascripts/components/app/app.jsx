import React, { Fragment, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route, Link, Switch } from 'react-router-dom';

import Modal from 'components/modal';
import HelloWorld from 'components/hello-world';
import visuallyHidden from 'constants/styles';
import { Loader, Device } from 'components/layout';
import { toggleModal, reveal } from './animations';

const initialModalStyle = {
  display: 'none',
  opacity: 0,
};

const App = ({ showModal }) => {
  const page = useRef();
  const modal = useRef();
  const firstUpdate = useRef(true);

  useEffect(() => {
    reveal(page.current);
  }, []);

  useEffect(() => {
    if (firstUpdate.current) {
      firstUpdate.current = false;
      return;
    }
    toggleModal(page.current, modal.current, showModal);
  }, [showModal]);

  return (
    <Fragment>
      <Device />
      <div className="page" ref={page} style={visuallyHidden}>
        <Switch>
          <Route exact path="/" component={HelloWorld} />
          <Redirect from="*" to="/" />
        </Switch>
      </div>
      <div className="page-modal" ref={modal} style={initialModalStyle}>
        <Modal />
      </div>
    </Fragment>
  );
};

App.propTypes = {
  showModal: PropTypes.bool.isRequired,
};

export default App;
