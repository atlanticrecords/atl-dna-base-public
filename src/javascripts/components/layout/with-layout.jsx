import React, { Fragment } from 'react';

import Footer from './footer';
import Header from './header';

const withLayout = Component => props => (
  <Fragment>
    <Header />
    <main className="content">
      <Component {...props} />
    </main>
    <Footer />
  </Fragment>
);

export default withLayout;
