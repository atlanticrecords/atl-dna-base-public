import React from 'react';
import ContestTerms from './contest-terms';

const InternationalTermsAgree = () => (
  <div id="mlist-terms-int" hidden="hidden">
    <div className="mlist-field full">
      <label htmlFor="id_int_terms_problem_check">What is four plus four?</label>
      <input
        type="text"
        id="id_int_terms_problem_check"
        name="int_terms_problem_check"
        placeholder="Answer here"
        disabled="disabled"
      />
      <div className="messages" />
    </div>
    <div className="mlist-field full">
      <div className="mlist-checkbox">
        <input
          className="checkbox"
          id="id_int_termsagree_1"
          type="checkbox"
          name="int_termsagree_1"
          value=""
          disabled="disabled"
        />
        <label htmlFor="id_int_termsagree_1">
          <span className="ui"></span>
        </label>
      </div>
      <ContestTerms />
      <div className="messages" />
    </div>
    <div className="mlist-field full">
      <div className="mlist-checkbox">
        <input
          className="checkbox"
          id="id_int_termsagree_2"
          type="checkbox"
          name="int_termsagree_2"
          value=""
          disabled="disabled"
        />
        <label htmlFor="id_int_termsagree_2" />
      </div>
      <ContestTerms />
      <div className="messages" />
    </div>
  </div>
);

export default InternationalTermsAgree;
