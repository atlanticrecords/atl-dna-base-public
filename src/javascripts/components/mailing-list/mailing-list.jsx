import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import FormContainer from './forms/form-container';
import SecondaryForm from './forms/secondary-form';
import { scrapeEmail } from 'utils/mailing-list';

const MailingList = ({ children, includeSecondaryForm = false, onSuccess }) => {
  const [showPrimaryForm, setShowPrimaryForm] = useState(true);
  const [showSecondaryForm, setShowSecondaryForm] = useState(false);
  const [showThankYou, setShowThankYou] = useState(false);

  return (
    <div className="mlist">
      {showPrimaryForm ? (
        <div className="mlist__primary-form">
        </div>
      ) : null}
      {includeSecondaryForm ? (
        <div className="mlist__secondary-form" style={{display: showSecondaryForm ? 'block' : 'none'}}>
          <SecondaryForm />
        </div>
      ) : null}
      {showThankYou ? (
        <div className="mlist__thank-you">
          <p>Thank you!</p>
        </div>
      ) : null}
    </div>
  );
};

MailingList.defaultProps = {
};

MailingList.propTypes = {
  children: PropTypes.node.isRequired,
  includeSecondaryForm: PropTypes.bool,
};

export default MailingList;
