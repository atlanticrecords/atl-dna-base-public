import React from 'react';
import PropTypes from 'prop-types';

const SubmitBtn = ({ btnText }) => (
  <div className="mlist-primary-submit">
    <button className="btn" id="submit" type="submit">
      {btnText}
    </button>
  </div>
);

SubmitBtn.defaultProps = {
  btnText: 'Submit'
};

SubmitBtn.propTypes = {
  btnText: PropTypes.string.isRequired
};

export default SubmitBtn;
