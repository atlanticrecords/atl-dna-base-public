import HelloWorld from 'components/hello-world';

const modalTypes = {
  HelloWorld: HelloWorld,
};

export default modalTypes;
